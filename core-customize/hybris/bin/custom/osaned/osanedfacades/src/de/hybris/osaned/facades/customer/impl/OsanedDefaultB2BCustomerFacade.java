/**
 *
 */
package de.hybris.osaned.facades.customer.impl;

import de.hybris.osaned.core.customer.OsanedB2BCustomerAccountService;
import de.hybris.osaned.facades.customer.OsanedB2BCustomerFacade;
import de.hybris.platform.b2bacceleratorfacades.customer.impl.DefaultB2BCustomerFacade;
import de.hybris.platform.commercefacades.user.data.B2BCustomerData;
import de.hybris.platform.commerceservices.customer.DuplicateUidException;


/**
 * @author bala.v
 *
 */
public class OsanedDefaultB2BCustomerFacade extends DefaultB2BCustomerFacade implements OsanedB2BCustomerFacade
{

	private OsanedB2BCustomerAccountService osanedB2BCustomerAccountService;

	@Override
	public void registerB2BCustomer(final B2BCustomerData b2bCustomerData) throws DuplicateUidException
	{
		if (!getUserFacade().isUserExisting(b2bCustomerData.getEmail()))
		{
			getOsanedB2BCustomerAccountService().registerB2BCustomer(b2bCustomerData);
		}
		else
		{
			throw new DuplicateUidException("Customer already exists : " + b2bCustomerData.getEmail());
		}

	}

	/**
	 * @return the osanedB2BCustomerAccountService
	 */
	public OsanedB2BCustomerAccountService getOsanedB2BCustomerAccountService()
	{
		return osanedB2BCustomerAccountService;
	}

	/**
	 * @param osanedB2BCustomerAccountService
	 *           the osanedB2BCustomerAccountService to set
	 */
	public void setOsanedB2BCustomerAccountService(final OsanedB2BCustomerAccountService osanedB2BCustomerAccountService)
	{
		this.osanedB2BCustomerAccountService = osanedB2BCustomerAccountService;
	}


}
