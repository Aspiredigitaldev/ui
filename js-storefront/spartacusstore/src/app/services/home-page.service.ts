import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { CommonUtilityService } from '../services/common-utility-service';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root',
})
export class HomePageService {
  constructor(
    private http: HttpClient,
    private utilityService: CommonUtilityService
  ) {}
  postUserMessage(req) {
    const url = '';
    return this.http.post(url, req);
  }
}
