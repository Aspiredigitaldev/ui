import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-home-organism',
  templateUrl: './home-organism.component.html',
  styleUrls: ['./home-organism.component.scss'],
})
export class HomeOrganismComponent implements OnInit {
  homeContent: any;

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.data.subscribe((data: { homeData: any }) => {
      const response = data.homeData;
      if (
        response &&
        response.contentSlots &&
        response.contentSlots.contentSlot
      ) {
        for (const content of response.contentSlots.contentSlot) {
          if (content.slotId === 'HomePageCarouselSlot') {
            this.homeContent = content.components.component;
          }
        }
      }
    });
  }
}
