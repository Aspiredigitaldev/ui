import { Component, OnInit, Input } from '@angular/core';
import { LoginService } from '../../../services/login.service'
import { CommonUtilityService } from '../../../services/common-utility-service'


@Component({
  selector: 'app-cx-header-links-molecule',
  templateUrl: './header-links-molecule.component.html',
  styleUrls: ['./header-links-molecule.component.scss']
})
export class HeaderLinksMoleculeComponent implements OnInit {

  @Input() headerLinks: any;
  displayName: any = '';
  isLoggedIn: any;
  userContext: any;

  constructor(
    private loginService: LoginService,
    private utilityService: CommonUtilityService) {
    this.isLoggedIn = false;
    this.loginService.userContext.subscribe(context => {
      this.userContext = context;
    });
  }

  ngOnInit() {
    this.loginService.userContext.subscribe(context => {
      this.displayName = context.displayName;
      this.isLoggedIn = context.displayName ? true : false
    });
  }

  onLogout(e): void {

    e.preventDefault()
    
    this.userContext.displayUID = ''
    this.userContext.displayName = ''
    this.userContext.isAuthenticated = false

    this.utilityService.removeCookie('isAuthenticated')
    this.utilityService.removeCookie('displayUid')
    this.utilityService.removeCookie('userName')

    this.loginService.changeUserContext(this.userContext)
  }

}
