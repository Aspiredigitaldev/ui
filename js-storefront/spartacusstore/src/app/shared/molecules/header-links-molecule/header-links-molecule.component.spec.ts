import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderLinksMoleculeComponent } from './header-links-molecule.component';

describe('HeaderLinksMoleculeComponent', () => {
  let component: HeaderLinksMoleculeComponent;
  let fixture: ComponentFixture<HeaderLinksMoleculeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HeaderLinksMoleculeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderLinksMoleculeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
