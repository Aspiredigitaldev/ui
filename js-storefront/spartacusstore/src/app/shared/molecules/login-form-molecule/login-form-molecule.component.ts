import { Component, OnInit } from '@angular/core';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
} from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../../../services/login.service';
import { CommonUtilityService } from '../../../services/common-utility-service';

@Component({
  selector: 'app-login-form-molecule',
  templateUrl: './login-form-molecule.component.html',
  styleUrls: ['./login-form-molecule.component.scss'],
})
export class LoginFormMoleculeComponent implements OnInit {
  userContext: any;
  loginError: boolean = false;
  userLoginForm: FormGroup;
  validationMessages = {
    email: [
      { type: 'required', message: 'Email is required' },
      { type: 'email', message: 'Enter a valid email' },
    ],
    password: [{ type: 'required', message: 'Password is required' }],
    message: [{ type: 'required', message: 'Please enter some message' }],
  };

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private router: Router,
    private utilityService: CommonUtilityService
  ) {
    this.loginService.userContext.subscribe(context => {
      this.userContext = context;
    });
  }

  ngOnInit() {
    // user details form validations
    this.userLoginForm = this.fb.group({
      email: [
        null,
        Validators.compose([Validators.required, Validators.email]),
      ],
      password: [null, Validators.compose([Validators.required])],
    });
  }
  onSubmitLoginDetails(userData) {
    console.log('userValue', userData);
    if (!!userData && !!userData.email && !!userData.password) {
      this.loginService.userLogin(userData).subscribe(
        (res: any) => {
          if (!!res && res.access_token) {
            this.loginService
              .fetchUserDetails(res.access_token, userData.email)
              .subscribe(
                (res: any) => {
                  if (res && !res.active) {
                    this.userContext.displayUID = res.displayUid;
                    this.userContext.displayName = res.firstName;
                    this.userContext.isAuthenticated = true;

                    this.loginService.changeUserContext(this.userContext);

                    this.utilityService.setCookie(
                      'userName',
                      this.userContext.displayName
                    );
                    this.utilityService.setCookie(
                      'displayUid',
                      this.userContext.displayUID
                    );
                    this.utilityService.setCookie('isAuthenticated', true);

                    this.router.navigate(['/']);
                  }
                },
                err => {
                  this.loginError = true;
                }
              );
          } else {
            this.loginError = true;
          }
        },
        err => {
          this.loginError = true;
        }
      );
    }
  }
}
