import { Component, OnInit, Input } from '@angular/core';
import { environment } from '../../../../environments/environment';

@Component({
  selector: 'app-logo-molecule',
  templateUrl: './logo-molecule.component.html',
  styleUrls: ['./logo-molecule.component.scss']
})
export class LogoMoleculeComponent implements OnInit {

  @Input() displayType: any;
  @Input() logoUrl: any;

  hostName: any = environment.hostName;
  constructor() { }

  ngOnInit() {
  }

}
