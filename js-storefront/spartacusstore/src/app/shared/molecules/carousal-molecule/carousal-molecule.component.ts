import { Component, Input } from '@angular/core';
import { environment } from '../../../../environments/environment'

@Component({
  selector: 'cx-carousal-molecule',
  templateUrl: './carousal-molecule.component.html',
  styleUrls: ['./carousal-molecule.component.scss']
})

export class CarousalMoleculeComponent {
  @Input() homeContent: any;
  hostName: String = environment.hostName;
}
