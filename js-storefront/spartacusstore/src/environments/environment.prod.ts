export const environment = {
  production: true,
  hostName: 'https://api.c050ygx6-obeikanin2-s1-public.model-t.cc.commerce.ondemand.com/',
  headerEndpoint: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=headerpage',
  logoUrl: 'medias/site-logo.png?context=bWFzdGVyfGltYWdlc3wxMTEyOXxpbWFnZS9wbmd8aDUyL2gxNi84Nzk2NzE3MjUyNjM4L3NpdGUtbG9nby5wbmd8MTJiMzZkYjBlZDNkMTVjMWMwOGMyMTU0NTlkZGI3NjE3NGU3MGFkZmVhNjg2M2VkY2E2ZGRkODA5MDZmNjMyOQ',
  homePageEndPoint: '/osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=homepage',
  oAuthAPI: 'authorizationserver/oauth/token',
  registerUserAPI: 'osanedcommercewebservices/v2/osaned/users',
  footerEndPoint: 'osanedcommercewebservices/v2/osaned/cms/pages?pageType=ContentPage&pageLabelOrId=footerPage',
  tokenEndpoint: 'authorizationserver/oauth/token',
  loginEndpoint: 'osanedcommercewebservices/v2/osaned/users/'
};
